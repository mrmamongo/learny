import logging

import miniopy_async.error
from sqlalchemy.exc import DisconnectionError, InvalidRequestError, OperationalError
from tenacity import (
    RetryCallState,
    retry,
    retry_if_exception_type,
    stop_after_attempt,
    wait_exponential_jitter,
    wait_fixed,
)

logger = logging.getLogger()


def _retry_logging(retry_state: RetryCallState) -> None:
    if (not retry_state.outcome) or (not retry_state.fn):
        return
    logger.error(
        f"Function {retry_state.fn.__name__} failed, "
        f"{repr(retry_state.outcome.exception())}. "
        f"Retrying attempt: {retry_state.attempt_number},"
        f"time_spend: {retry_state.idle_for}"
    )


psql_session_retry = retry(
    wait=wait_fixed(1),
    stop=stop_after_attempt(30),
    retry=retry_if_exception_type(
        (OperationalError, InvalidRequestError, DisconnectionError)
    ),
    after=_retry_logging,
)

psql_connection_retry = retry(
    wait=wait_exponential_jitter(1, 60),
    retry=retry_if_exception_type(OperationalError),
    after=_retry_logging,
)

minio_connection_retry = retry(
    wait=wait_exponential_jitter(0.1, 10),
    retry=retry_if_exception_type(miniopy_async.ServerError),
    after=_retry_logging,
)
