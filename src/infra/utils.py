import inspect
from typing import Type

from fastapi import Form
from pydantic import BaseModel

from src.infra.database.models.base import Base


def entity_update(entity: Base, updater: BaseModel) -> None:
    for key, value in updater.dict(exclude_none=True).items():
        setattr(entity, key, value)
