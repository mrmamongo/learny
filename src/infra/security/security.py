from fastapi.security import OAuth2PasswordBearer
from passlib.context import CryptContext

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl="api/v1/auth/sign_in",
    scopes={
        "admin": "admin access",
        "moderator": "moderator access",
        "user": "user access",
        "banned": "banned access",
    },
)
