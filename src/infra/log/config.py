from pathlib import Path

from pydantic import BaseModel


class LoggingConfig(BaseModel):
    render_json_logs: bool = False
    path: Path | None = None
    level: str = "DEBUG"
