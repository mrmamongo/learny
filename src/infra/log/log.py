import logging

from src.infra.log.config import LoggingConfig


def configure_logging(config: LoggingConfig):
    logging.basicConfig(level=config.level)
