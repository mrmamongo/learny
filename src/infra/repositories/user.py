import logging
from uuid import UUID

from nats.js.kv import KeyValue
from sqlalchemy.ext.asyncio import AsyncSession

from src.application.common.exceptions import RepoError
from src.application.user.dto import User, Users
from src.application.user.interfaces.user_mutator import UserMutator
from src.application.user.interfaces.user_reader import GetUsersFilters, UserReader
from src.infra.database.repositories.user import (
    UserReaderSQLAlchemyRepo,
    UserMutatorSQLAlchemyRepo,
)
from src.infra.nats.cache.repositories.user import (
    UserMutatorCacheRepo,
    UserReaderCacheRepo,
)


class UserMutatorMixin(UserMutator):
    logger: logging.Logger
    cache_mutator: UserMutatorCacheRepo
    db_mutator: UserMutatorSQLAlchemyRepo

    def __init__(self, cache: KeyValue, db: AsyncSession):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.cache_mutator = UserMutatorCacheRepo(cache)
        self.db_mutator = UserMutatorSQLAlchemyRepo(db)

    async def acquire_user_by_id(self, user_id: UUID) -> User:
        if cached_user := await self.cache_mutator.acquire_user_by_id(user_id):
            return cached_user

        user = await self.db_mutator.acquire_user_by_id(user_id)
        await self.cache_mutator.add_user(user)
        return user

    async def add_user(self, user: User) -> None:
        try:
            await self.db_mutator.add_user(user)
        except RepoError as e:
            self.logger.error(e)
            raise e
        await self.cache_mutator.add_user(user)

    async def update_user(self, user: User) -> None:
        await self.db_mutator.update_user(user)
        await self.cache_mutator.update_user(user)

    async def delete_user(self, user_id: UUID) -> None:
        await self.db_mutator.delete_user(user_id)
        await self.cache_mutator.delete_user(user_id)

    async def check_user_exists(self, user_id: UUID) -> bool:
        if in_cache := await self.cache_mutator.check_user_exists(user_id):
            return in_cache

        if in_db := await self.db_mutator.check_user_exists(user_id):
            user = await self.db_mutator.acquire_user_by_id(user_id)
            await self.cache_mutator.add_user(user)
            return in_db

        return False

    async def check_username_exists(self, username: str) -> bool:
        return await self.db_mutator.check_username_exists(username)


class UserReaderMixin(UserReader):
    cache_reader: UserReaderCacheRepo
    cache_mutator: UserMutatorCacheRepo
    db_reader: UserReaderSQLAlchemyRepo

    def __init__(self, cache: KeyValue, db: AsyncSession):
        self.cache_mutator = UserMutatorCacheRepo(cache)
        self.cache_reader = UserReaderCacheRepo(cache)
        self.db_reader = UserReaderSQLAlchemyRepo(db)

    async def get_user_by_username(self, username: str) -> User | None:
        if cached_user := await self.cache_reader.get_user_by_username(username):
            return cached_user

        user = await self.db_reader.get_user_by_username(username)
        await self.cache_mutator.add_user(user)
        return user

    async def get_user_by_id(self, user_id: UUID) -> User | None:
        if cached_user := await self.cache_reader.get_user_by_id(user_id):
            return cached_user

        user = await self.db_reader.get_user_by_id(user_id)
        await self.cache_mutator.add_user(user)
        return user

    async def get_users(self, filters: GetUsersFilters) -> Users:
        users = await self.db_reader.get_users(filters)
        return users

    async def get_users_count(self, deleted: bool = False) -> int:
        return await self.db_reader.get_users_count(deleted)
