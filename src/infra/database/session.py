from contextlib import asynccontextmanager
from typing import AsyncContextManager, Callable, cast

from sqlalchemy.ext.asyncio import (
    AsyncEngine,
    AsyncSession,
    async_sessionmaker,
    create_async_engine,
)

from src.infra.database.config import DatabaseConfig


def setup_database(
    config: DatabaseConfig,
) -> tuple[Callable[[], AsyncContextManager[AsyncSession]], AsyncEngine]:
    engine = create_async_engine(
        cast(str, config.dsn),
        pool_pre_ping=True,
    )

    session_local = async_sessionmaker(
        bind=engine,
        autocommit=False,
        autoflush=False,
        expire_on_commit=False,
        class_=AsyncSession,
    )

    @asynccontextmanager
    async def session() -> AsyncContextManager[AsyncSession]:
        async with session_local() as session_:
            try:
                yield session_
                await session_.commit()
            except Exception as e:
                await session_.rollback()
                raise e

    return session, engine
