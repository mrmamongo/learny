from uuid import UUID

from sqlalchemy import func, select
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession

from src.application.user.dto.user import User as UserDTO
from src.application.user.dto.users import Users as UsersDTO, UserDTOs
from src.application.user.interfaces.user_mutator import UserMutator
from src.application.user.interfaces.user_reader import (
    GetUsersFilters,
    GetUsersOrder,
    UserReader,
)
from src.infra.database import converters
from src.infra.database.models.user import User as UserInDB
from src.infra.database.repositories.base import SQLAlchemyRepo


class UserReaderSQLAlchemyRepo(SQLAlchemyRepo, UserReader):
    def __init__(self, session: AsyncSession) -> None:
        super().__init__(session)

    async def get_user_by_username(self, username: str) -> UserDTOs | None:
        user = (
            await self.session.scalars(
                select(UserInDB).where(UserInDB.username == username).limit(1)
            )
        ).one_or_none()
        return converters.db_model_to_dto(user)

    async def get_user_by_id(self, user_id: UUID) -> UserDTOs | None:
        user = await self.session.get(UserInDB, user_id)
        return converters.db_model_to_dto(user)

    async def get_users(self, filters: GetUsersFilters) -> UsersDTO:
        query = select(UserInDB)
        if filters.order is GetUsersOrder.ASC:
            query = query.order_by(UserInDB.id.asc())
        else:
            query = query.order_by(UserInDB.id.desc())

        if filters.deleted is not None:
            query = query.where(UserInDB.deleted == filters.deleted)

        if filters.offset is not None:
            query = query.offset(filters.offset)

        if filters.limit is not None:
            query = query.limit(filters.limit)

        result = await self.session.scalars(query)
        users_list = [converters.db_model_to_dto(user) for user in result]
        return UsersDTO(
            users=users_list,
            total=len(users_list),
            offset=filters.offset,
            limit=filters.limit,
        )

    async def get_users_count(self, deleted: bool = False) -> int:
        return (
            await self.session.scalar(select(func.count()).select_from(UserInDB))
        ) or 0


class UserMutatorSQLAlchemyRepo(SQLAlchemyRepo, UserMutator):
    def __init__(self, session: AsyncSession):
        super().__init__(session)

    async def acquire_user_by_id(self, user_id: UUID) -> UserDTOs:
        user = await self.session.get(UserInDB, user_id)
        return converters.db_model_to_dto(user)

    async def add_user(self, user: UserDTO) -> None:
        entity: UserInDB = converters.dto_to_db_model(user)
        try:
            self.session.add(entity)
            await self.session.commit()
            await self.session.refresh(entity)
        except IntegrityError:
            raise

    async def update_user(self, user: UserDTO) -> None:
        entity: UserInDB | None = await self.session.get(UserInDB, user.id)

        for key, item in user.dict(exclude_none=True).items():
            if hasattr(entity, key):
                setattr(entity, key, item)

        await self.session.commit()
        await self.session.refresh(entity)

    async def delete_user(self, user_id: UUID) -> None:
        await self.session.delete(await self.session.get(UserInDB, user_id))

    async def check_user_exists(self, user_id: UUID) -> bool:
        return (
            await self.session.scalar(
                select(func.count()).select_from(UserInDB).where(UserInDB.id == user_id)
            )
        ).one_or_none() == 1

    async def check_username_exists(self, username: str) -> bool:
        return (
            await self.session.scalar(
                select(func.count())
                .select_from(UserInDB)
                .where(UserInDB.username == username)
            )
        ).fetchone() == 1
