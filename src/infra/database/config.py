from typing import Optional, cast

from pydantic import BaseModel, PostgresDsn, validator


class DatabaseConfig(BaseModel):
    server: str
    port: str
    user: str
    password: str
    db: str
    dsn: Optional[PostgresDsn] = None

    @validator("dsn", always=True)
    def assemble_db_connection(cls, v: Optional[str], values: dict[str, str]) -> str:
        if isinstance(v, str):
            return v
        return cast(
            str,
            PostgresDsn.build(
                scheme="postgresql+asyncpg",
                user=values.get("user"),
                password=values.get("password"),
                host=values.get("server"),
                port=values.get("port"),
                path=f"/{values.get('db') or ''}",
            ),
        )
