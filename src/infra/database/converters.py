from src.application.user import dto
from src.infra.database import models


def dto_to_db_model(user: dto.User) -> models.User:
    return models.User(
        id=user.id,
        username=user.username,
        password=user.password,
        email=user.email,
        created_at=user.created_at,
        role=user.role,
        first_name=user.first_name,
        last_name=user.last_name,
        birth_date=user.birth_date,
    )


def db_model_to_dto(user: models.User | None) -> dto.User | None:
    if user is None:
        return user
    return dto.User(
        id=user.id,
        username=user.username,
        email=user.email,
        password=user.password,
        created_at=user.created_at,
        role=user.role,
        first_name=user.first_name,
        last_name=user.last_name,
        birth_date=user.birth_date,
    )
