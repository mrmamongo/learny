import enum
import uuid

from datetime import datetime

from sqlalchemy import UUID, Enum, func
from sqlalchemy.orm import Mapped, mapped_column

from src.infra.database.models.base import Base


class Role(enum.Enum):
    ADMIN = "admin"
    MODERATOR = "moderator"
    USER = "user"
    BANNED = "banned"


class User(Base):
    id: Mapped[uuid.UUID] = mapped_column(
        UUID(as_uuid=True),
        primary_key=True,
        default=uuid.uuid4(),
    )
    username: Mapped[str] = mapped_column(nullable=False, unique=True)
    password: Mapped[str] = mapped_column(nullable=False)
    email: Mapped[str] = mapped_column(nullable=False)
    created_at: Mapped[datetime] = mapped_column(nullable=False, default=func.now())

    role: Mapped[Role] = mapped_column(Enum(Role), nullable=False, default=Role.USER)
    deleted: Mapped[bool] = mapped_column(nullable=False, default=False)

    first_name: Mapped[str] = mapped_column(nullable=True)
    last_name: Mapped[str] = mapped_column(nullable=True)
    birth_date: Mapped[datetime] = mapped_column(nullable=True)
