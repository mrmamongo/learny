import asyncio
import logging
from typing import Callable, Coroutine, Any

import nats
from nats.aio.client import Client
from nats.js import JetStreamContext
from nats.js.errors import NotFoundError
from nats.js.kv import KeyValue
from nats.js.object_store import ObjectStore

from src.infra.nats.cache.config import CacheConfig
from src.infra.nats.config import NATSConfig, BrokerConfig
from src.infra.nats.object_store.config import ObjectStoreConfig

logger = logging.getLogger("nats")
JetStreamGetter = Callable[[], JetStreamContext]
KeyValueGetter = Callable[[], Coroutine[Any, Any, KeyValue]]
ObjectStoreGetter = Callable[[], Coroutine[Any, Any, ObjectStore]]


async def setup_cache(config: CacheConfig, js: JetStreamContext) -> None:
    try:
        await js.key_value(config.bucket)
    except NotFoundError:
        await js.create_key_value(**config.dict(exclude_none=True))


async def setup_object_store(config: ObjectStoreConfig, js: JetStreamContext) -> None:
    try:
        await js.object_store(config.bucket)
    except NotFoundError:
        await js.create_object_store(**config.dict(exclude_none=True))


async def setup_broker(config: BrokerConfig, js: JetStreamContext) -> None:
    try:
        await js.stream_info(config.name)
    except NotFoundError:
        await js.add_stream(**config.dict(exclude_none=True))


async def setup_nats(
    config: NATSConfig,
) -> tuple[JetStreamGetter, KeyValueGetter, ObjectStoreGetter, Client]:
    nc = await nats.connect(config.urls)

    js = nc.jetstream()

    await asyncio.gather(
        setup_cache(config.cache, js),
        setup_object_store(config.object_store, js),
        setup_broker(config.broker, js),
    )

    def get_broker():
        return nc.jetstream()

    async def get_cache():
        return await nc.jetstream().key_value(config.cache.bucket)

    async def get_object_store():
        return await nc.jetstream().object_store(config.object_store.bucket)

    return get_broker, get_cache, get_object_store, nc
