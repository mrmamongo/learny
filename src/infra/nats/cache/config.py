from typing import Optional

from nats.js.api import StorageType
from pydantic import BaseModel


class CacheConfig(BaseModel):
    bucket: str
    ttl: float = 3600  # in seconds
    history: int = 1
    replicas: int = 1
    description: Optional[str] = None
    max_bytes: Optional[int] = None
    storage: Optional[StorageType] = None
