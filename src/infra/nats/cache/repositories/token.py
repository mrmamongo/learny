import uuid
from datetime import timedelta

from nats.js.errors import KeyNotFoundError

from src.application.user.interfaces.tokens import RefreshTokenRepo
from src.infra.nats.cache.repositories.base import CacheRepo


class RefreshTokenCacheRepo(CacheRepo, RefreshTokenRepo):
    async def set_token(
        self, user_id: uuid.UUID, token: str, expire_time: timedelta
    ) -> None:
        try:
            cached_token = await self.cache.get(str(user_id))
            await self.cache.update(
                cached_token.key, token.encode("utf-8"), cached_token.revision
            )
        except KeyNotFoundError:
            await self.cache.create(str(user_id), token.encode("utf-8"))

    async def get_token(self, user_id: uuid.UUID) -> str | None:
        try:
            return (await self.cache.get(str(user_id))).value.decode("utf-8")
        except KeyNotFoundError:
            return None

    async def delete_token(self, user_id: uuid.UUID) -> None:
        try:
            cached_token = await self.cache.get(str(user_id))
            await self.cache.delete(cached_token.key)
        except KeyNotFoundError:
            return None
