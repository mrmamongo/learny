from uuid import UUID

from nats.js.errors import KeyNotFoundError

from src.application.user.dto import UserDTOs, User, Users
from src.application.user.interfaces.user_mutator import UserMutator
from src.application.user.interfaces.user_reader import UserReader, GetUsersFilters
from src.infra.nats.cache.repositories.base import CacheRepo


class UserReaderCacheRepo(CacheRepo, UserReader):
    async def get_user_by_username(self, username: str) -> UserDTOs | None:
        try:
            if (user := await self.cache.get(username)) is not None:
                return User.from_json(user.value)
            return None
        except KeyNotFoundError:
            return None

    async def get_user_by_id(self, user_id: UUID) -> UserDTOs | None:
        try:
            if (user := await self.cache.get(str(user_id))) is not None:
                return User.from_json(user.value)
            return None
        except KeyNotFoundError:
            return None

    async def get_users(self, filters: GetUsersFilters) -> Users:
        # TODO: Implement caching all users
        return Users(users=[], total=0)

    async def get_users_count(self, deleted: bool = False) -> int:
        return 0


class UserMutatorCacheRepo(CacheRepo, UserMutator):
    async def acquire_user_by_id(self, user_id: UUID) -> User:
        if (user := await self.cache.get(str(user_id))) is not None:
            return User.from_json(user.value)

    async def add_user(self, user: User) -> None:
        try:
            await self.update_user(user)
        except KeyNotFoundError:
            await self.cache.create(str(user.id), user.to_json(exclude_none=True))

    async def update_user(self, user: User) -> None:
        try:
            if (cached_user := await self.cache.get(str(user.id))) is not None:
                await self.cache.update(
                    cached_user.key,
                    user.to_json(exclude_none=True),
                    cached_user.revision,
                )
                return None

            await self.cache.create(str(user.id), user.to_json(exclude_none=True))
        except KeyNotFoundError:
            return None

    async def delete_user(self, user_id: UUID) -> None:
        try:
            if (cached_user := await self.cache.get(str(user_id))) is not None:
                await self.cache.delete(cached_user.key, cached_user.revision)
        except KeyNotFoundError:
            return None

    async def check_user_exists(self, user_id: UUID) -> bool:
        try:
            return (await self.cache.get(str(user_id))) is not None
        except KeyNotFoundError:
            return False

    async def check_username_exists(self, username: str) -> bool:
        try:
            return (await self.cache.get(str(username))) is not None
        except KeyNotFoundError:
            return False
