from nats.js.kv import KeyValue
from nats.js.object_store import ObjectStore


class CacheRepo:
    def __init__(self, cache: KeyValue):
        self.cache = cache


class ObjectStoreRepo:
    def __init__(self, object_store: ObjectStore):
        self.object_store = object_store
