from nats.js.object_store import ObjectStore


class ObjectStoreRepo:
    def __init__(self, object_store: ObjectStore):
        self.object_storage = object_store
