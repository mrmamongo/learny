from typing import Optional

from nats.js.api import StorageType
from pydantic import BaseModel


class ObjectStoreConfig(BaseModel):
    bucket: str
    ttl: float = 3600  # in seconds
    replicas: int = 1
    description: Optional[str] = None
    max_bytes: Optional[int] = None
    storage: Optional[StorageType] = None
