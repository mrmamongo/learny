from typing import Optional

from nats.js.api import StorageType, RetentionPolicy
from pydantic import BaseModel, validator

from src.infra.nats.cache.config import CacheConfig
from src.infra.nats.object_store.config import ObjectStoreConfig


class BrokerConfig(BaseModel):
    name: str
    subjects: list[str] = []
    max_msgs_per_subject: int = -1
    max_msg_size: Optional[int] = -1
    retention: Optional[RetentionPolicy] = RetentionPolicy.LIMITS
    description: Optional[str] = None
    max_consumers: Optional[int] = None
    max_msgs: Optional[int] = None
    max_bytes: Optional[int] = None
    max_age: Optional[float] = None  # in seconds
    storage: Optional[StorageType] = None

    @validator("subjects", pre=True)
    def validate_subjects(cls, v: str | list[str]) -> list[str] | str:
        if isinstance(v, str) and v.startswith("["):
            return [i.strip().replace("[", "").replace("]", "") for i in v.split(",")]
        elif isinstance(v, list):
            return v
        raise ValueError(v)


class NATSConfig(BaseModel):
    cache: CacheConfig
    object_store: ObjectStoreConfig
    broker: BrokerConfig

    name: str
    urls: list[str] = []

    @validator("urls", pre=True)
    def validate_nats_urls(cls, v: str | list[str]) -> list[str] | str:
        if isinstance(v, str) and v.startswith("["):
            return [i.strip().replace("[", "").replace("]", "") for i in v.split(",")]
        elif isinstance(v, list):
            return v
        raise ValueError(v)
