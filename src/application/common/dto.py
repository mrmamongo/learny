import uuid
from dataclasses import asdict, dataclass
from typing import Protocol, TypeVar, Any

from orjson import orjson
from sqlalchemy.dialects.postgresql import asyncpg

from src.application.adaptix.adapter import retort
from src.infra.database.models import Base
import asyncpg.pgproto.pgproto

T = TypeVar("T", bound=Base)


def converter(obj: T):
    if isinstance(obj, uuid.UUID):
        return str(obj)
    if isinstance(obj, asyncpg.pgproto.pgproto.UUID):
        return str(obj)
    return obj


@dataclass(frozen=True)
class DTO(Protocol):
    @classmethod
    def from_dict(cls, data: dict[str, Any]) -> "DTO":
        return retort.load(data, cls)

    def dict(self, exclude_none: bool = False) -> dict:  # type: ignore
        if exclude_none:
            return asdict(
                self, dict_factory=lambda x: {k: v for (k, v) in x if v is not None}
            )
        return asdict(self)

    def to_json(self, exclude_none: bool = False) -> bytes:
        return orjson.dumps(self.dict(exclude_none), default=converter)

    @classmethod
    def from_json(cls, json: bytes | bytearray) -> "DTO":
        return retort.load(orjson.loads(json), cls)
