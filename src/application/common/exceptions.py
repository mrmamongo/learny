class ApplicationException(Exception):
    @property
    def message(self) -> str:
        return "An application error occurred"


class UnexpectedError(ApplicationException):
    pass


class CommitError(UnexpectedError):
    pass


class RollbackError(UnexpectedError):
    pass


class RepoError(UnexpectedError):
    pass


class MappingError(ApplicationException):
    pass
