import orjson
from pydantic import BaseModel


def orjson_dumps(v, *, default):  # type: ignore
    # orjson.dumps returns bytes, to match standard json.dumps we need to decode
    return orjson.dumps(v, default=default).decode()


class Base(BaseModel):
    class Config:
        json_loads = orjson.loads
        json_dumps = orjson_dumps
