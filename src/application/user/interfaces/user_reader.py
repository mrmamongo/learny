from dataclasses import dataclass
from enum import Enum
from typing import Protocol
from uuid import UUID

from src.application.user.dto import UserDTOs, Users


class GetUsersOrder(Enum):
    ASC = "asc"
    DESC = "desc"


@dataclass(frozen=True)
class GetUsersFilters:
    offset: int | None = None
    limit: int | None = None
    deleted: bool | None = None
    order: GetUsersOrder = GetUsersOrder.ASC


class UserReader(Protocol):
    async def get_user_by_username(self, username: str) -> UserDTOs | None:
        raise NotImplementedError

    async def get_user_by_id(self, user_id: UUID) -> UserDTOs | None:
        raise NotImplementedError

    async def get_users(self, filters: GetUsersFilters) -> Users:
        raise NotImplementedError

    async def get_users_count(self, deleted: bool = False) -> int:
        raise NotImplementedError
