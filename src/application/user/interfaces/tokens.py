import uuid
from datetime import timedelta
from typing import Protocol


class RefreshTokenRepo(Protocol):
    async def set_token(
        self, user_id: uuid.UUID, token: str, expire_time: timedelta
    ) -> None:
        raise NotImplementedError

    async def get_token(self, user_id: uuid.UUID) -> str | None:
        raise NotImplementedError

    async def delete_token(self, user_id: uuid.UUID) -> None:
        raise NotImplementedError
