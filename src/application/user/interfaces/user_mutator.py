from typing import Protocol
from uuid import UUID

from src.application.user.dto import User


class UserMutator(Protocol):
    async def acquire_user_by_id(self, user_id: UUID) -> User:
        raise NotImplementedError

    async def add_user(self, user: User) -> None:
        raise NotImplementedError

    async def update_user(self, user: User) -> None:
        raise NotImplementedError

    async def delete_user(self, db_user) -> None:
        raise NotImplementedError

    async def check_user_exists(self, user_id: UUID) -> bool:
        raise NotImplementedError

    async def check_username_exists(self, username: str) -> bool:
        raise NotImplementedError
