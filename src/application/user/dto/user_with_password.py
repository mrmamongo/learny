from dataclasses import dataclass

from src.application.user.dto import User


@dataclass(frozen=True)
class UserWithPassword(User):
    hashed_password: str
