from dataclasses import dataclass

from src.application.user.dto.user import User

UserDTOs = User  # there shall be more DTO types


@dataclass(frozen=True)
class Users:
    users: list[UserDTOs]
    total: int
    offset: int | None = None
    limit: int | None = None
