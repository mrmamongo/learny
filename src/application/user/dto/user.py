from dataclasses import dataclass, field
from datetime import datetime
from uuid import UUID

from src.application.common.dto import DTO
from src.infra.database.models.user import Role


@dataclass(frozen=True)
class User(DTO):
    username: str
    email: str
    password: str
    id: UUID | None = None

    created_at: datetime = field(default_factory=datetime.now)
    role: Role = field(default=Role.USER)

    deleted: bool = field(default=False, init=False)

    first_name: str | None = None
    last_name: str | None = None
    birth_date: datetime | None = None
