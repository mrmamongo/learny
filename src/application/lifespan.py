import logging
from contextlib import asynccontextmanager
from typing import Any, AsyncContextManager, Mapping

from fastapi import FastAPI

from src.infra.log.log import configure_logging
from src.infra.database.session import setup_database
from src.infra.nats.session import setup_nats
from src.presentation.config import config


@asynccontextmanager  # type: ignore
async def lifespan(  # type: ignore
    app: FastAPI,
) -> AsyncContextManager[Mapping[str, Any]]:
    configure_logging(config.log)
    session_local, engine = setup_database(config.database)
    get_broker, get_cache, get_object_store, nats = await setup_nats(config.nats)
    logger = logging.getLogger("request")

    yield {
        "session_local": session_local,
        "broker": get_broker,
        "cache": get_cache,
        "object_store": get_object_store,
        "logger": logger,
    }

    await engine.dispose()

    await nats.close()
