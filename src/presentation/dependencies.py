from typing import Annotated

from fastapi import Request, Depends
from nats.js.kv import KeyValue
from nats.js.object_store import ObjectStore
from sqlalchemy.ext.asyncio import AsyncSession

from src.infra.repositories.user import UserReaderMixin, UserMutatorMixin


async def db_session_scope(request: Request) -> AsyncSession:
    async with getattr(request.state, "session_local", AsyncSession())() as session:
        return session


async def cache_session_scope(request: Request) -> KeyValue:
    return await request.state.cache()


async def minio_session_scope(request: Request) -> ObjectStore:
    return await request.state.object_store()


def get_user_reader(
    session: Annotated[AsyncSession, Depends(db_session_scope)],
    cache: Annotated[KeyValue, Depends(cache_session_scope)],
) -> UserReaderMixin:
    return UserReaderMixin(cache, session)


def get_user_mutator(
    session: Annotated[AsyncSession, Depends(db_session_scope)],
    cache: Annotated[KeyValue, Depends(cache_session_scope)],
) -> UserMutatorMixin:
    return UserMutatorMixin(cache, session)
