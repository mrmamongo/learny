from pydantic import AnyHttpUrl, BaseModel, BaseSettings, Field, validator

from src.infra.database.config import DatabaseConfig
from src.infra.log.config import LoggingConfig
from src.infra.nats.config import NATSConfig
from src.presentation.routers.auth.config import AuthConfig


class ApiConfig(BaseModel):
    prefix: str
    port: str
    project_name: str
    backend_cors_origins: list[AnyHttpUrl] = []

    @validator("backend_cors_origins", pre=True)
    def assemble_cors_origins(cls, v: str | list[str]) -> list[str] | str:
        if isinstance(v, str) and v.startswith("["):
            return [i.strip().replace("[", "").replace("]", "") for i in v.split(",")]
        elif isinstance(v, list):
            return v
        raise ValueError(v)


class Config(BaseSettings):
    api: ApiConfig = Field(default_factory=lambda **kwargs: ApiConfig(**kwargs))
    database: DatabaseConfig = Field(
        default_factory=lambda **kwargs: DatabaseConfig(**kwargs)
    )
    nats: NATSConfig = Field(default_factory=lambda **kwargs: NATSConfig(**kwargs))
    auth: AuthConfig = Field(default_factory=lambda **kwargs: AuthConfig(**kwargs))
    log: LoggingConfig = Field(default_factory=lambda **kwargs: LoggingConfig(**kwargs))

    class Config:
        env_file = ".env"
        env_nested_delimiter = "."


config = Config()
