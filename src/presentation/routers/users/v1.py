from typing import Annotated

from fastapi import APIRouter, Depends

from src.presentation.routers.users.dependencies import (
    delete_current_user,
    get_current_user,
    update_current_user,
    valid_user,
)
from src.presentation.routers.users.models import (
    UserRead,
    UserReadWithInfo,
    UserUpdate,
)

router = APIRouter(prefix="/users")


@router.get("/", response_model=UserRead)
async def get_user(user: Annotated[UserRead, Depends(valid_user)]) -> UserRead:
    return user


@router.get("/me/", response_model=UserReadWithInfo)
async def get_me(
    user: Annotated[UserReadWithInfo, Depends(get_current_user)]
) -> UserReadWithInfo:
    return user


@router.put("/me/", response_model=UserReadWithInfo)
async def update_user(
    user: Annotated[UserUpdate, Depends(update_current_user)]
) -> UserUpdate:
    return user


@router.delete("/me/", response_model=UserRead)
async def delete_user(
    user: Annotated[UserRead, Depends(delete_current_user)]
) -> UserRead:
    return user
