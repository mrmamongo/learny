import uuid

from fastapi import HTTPException, status


class UserNotFoundException(HTTPException):
    def __init__(self, user_id: uuid.UUID):
        super().__init__(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"User {user_id} not found",
        )


class BadUserUpdateException(HTTPException):
    def __init__(self) -> None:
        super().__init__(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY, detail="Bad user update"
        )
