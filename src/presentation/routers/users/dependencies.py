import uuid
from typing import Annotated

from fastapi import Depends

from src.application.user.interfaces.user_mutator import UserMutator
from src.application.user.interfaces.user_reader import UserReader
from src.infra.database import User
from src.presentation.dependencies import get_user_reader, get_user_mutator
from src.presentation.routers.auth.dependencies import validate_token
from src.presentation.routers.auth.exceptions import InvalidTokenException
from src.presentation.routers.auth.models import TokenData
from src.presentation.routers.users.exceptions import (
    BadUserUpdateException,
    UserNotFoundException,
)
from src.presentation.routers.users.models import UserUpdate


async def valid_user(
    user_id: uuid.UUID, reader: Annotated[UserReader, Depends(get_user_reader)]
) -> User:
    user = await reader.get_user_by_id(user_id)
    if user is None:
        raise UserNotFoundException(user_id)

    return user


async def get_current_user(
    user: Annotated[TokenData, Depends(validate_token)],
    reader: Annotated[UserReader, Depends(get_user_reader)],
) -> User:
    db_user = await reader.get_user_by_id(user.id)

    if db_user is None:
        raise InvalidTokenException
    return db_user


async def update_current_user(
    user_update: UserUpdate,
    user: Annotated[TokenData, Depends(validate_token)],
    mutator: Annotated[UserMutator, Depends(get_user_mutator)],
) -> User:
    db_user = await mutator.acquire_user_by_id(user.id)

    if db_user is None:
        raise InvalidTokenException

    if not user_update.is_valid():
        raise BadUserUpdateException

    await mutator.update_user(db_user)
    return db_user


async def delete_current_user(
    user: Annotated[TokenData, Depends(validate_token)],
    mutator: Annotated[UserMutator, Depends(get_user_mutator)],
) -> User:
    db_user = await mutator.acquire_user_by_id(user.id)

    if db_user is None:
        raise InvalidTokenException
    await mutator.delete_user(db_user)

    return db_user
