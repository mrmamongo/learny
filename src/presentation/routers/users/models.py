from datetime import datetime
from typing import Optional, cast

from pydantic import BaseModel, validator

from src.presentation.routers.auth.dependencies import pwd_context


class UserRead(BaseModel):
    username: str
    email: str
    created_at: datetime

    class Config:
        orm_mode = True


class UserReadWithInfo(UserRead):
    first_name: Optional[str]
    last_name: Optional[str]
    birth_date: Optional[datetime]

    class Config:
        orm_mode = True


class UserUpdate(BaseModel):
    username: Optional[str]
    password: Optional[str]
    email: Optional[str]

    first_name: Optional[str]
    last_name: Optional[str]
    birt_date: Optional[datetime]

    def is_valid(self) -> bool:
        return (
            self.username is not None
            or self.password is not None
            or self.email is not None
            or self.first_name is not None
            or self.last_name is not None
            or self.birt_date is not None
        )

    @validator("password")
    def hash_password(cls, v: str) -> str:
        if v is not None:
            return cast(str, pwd_context.hash(v))
