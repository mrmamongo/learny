from fastapi import APIRouter

router = APIRouter(prefix="/course")


@router.get("/")
def get_auth() -> str:
    return "course app created!"
