from typing import Annotated

from fastapi import APIRouter, Depends, status
from sqlalchemy.ext.asyncio import AsyncSession

from src.application.user.dto import UserDTOs
from src.presentation.dependencies import db_session_scope
from src.presentation.routers.auth.dependencies import (
    authenticate_user,
    create_token_pair,
    valid_refresh_token,
    validate_token,
    create_authenticate_user,
)
from src.presentation.routers.auth.models import Token, TokenData

router = APIRouter(prefix="/auth")


@router.post("/sign_in", response_model=Token)
async def sign_in(user: Annotated[UserDTOs, Depends(authenticate_user)]) -> Token:
    return await create_token_pair(
        user_id=user.id,
        scopes=[str(user.role)],
    )


@router.post("/sign_up", response_model=Token)
async def sign_up(
    user: Annotated[UserDTOs, Depends(create_authenticate_user)]
) -> Token:
    return await create_token_pair(
        user_id=user.id,
        scopes=[str(user.role)],
    )


@router.post("/sign_refresh", response_model=Token)
async def sign_refresh(
    valid_token: Annotated[Token, Depends(valid_refresh_token)],
) -> Token:
    return valid_token


@router.get("/sign_out")
async def sign_out(
    valid_user: Annotated[TokenData, Depends(validate_token)],
    db_session: Annotated[
        AsyncSession, Depends(db_session_scope)
    ],  # TODO: Reformat to depends cache_repo
) -> dict[str, str]:
    # TODO: await cache_repo.delete_token(valid_user.username)
    return {"status": status.HTTP_200_OK}
