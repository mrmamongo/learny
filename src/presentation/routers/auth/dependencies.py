import uuid
from datetime import datetime, timedelta
from typing import Annotated, cast

from fastapi import Depends
from jose import JWTError, jwt

from src.application.user.dto import UserDTOs, User
from src.application.user.interfaces.user_mutator import UserMutator
from src.application.user.interfaces.user_reader import UserReader
from src.infra.security.security import oauth2_scheme, pwd_context
from src.presentation.config import config
from src.presentation.dependencies import get_user_reader, get_user_mutator
from src.presentation.routers.auth.exceptions import (
    AuthBadCredentialsException,
    InvalidTokenException,
)
from src.presentation.routers.auth.models import Token, TokenData, UserLogin, UserCreate

ACCESS_TOKEN_EXPIRES = timedelta(minutes=config.auth.access_token_expire_minutes)

REFRESH_TOKEN_EXPIRES = timedelta(minutes=config.auth.refresh_token_expire_minutes)


async def create_authenticate_user(
    user_create: UserCreate, mutator: Annotated[UserMutator, Depends(get_user_mutator)]
) -> UserDTOs:
    user: UserDTOs = User.from_dict(user_create.dict())
    await mutator.add_user(user)

    return user


async def authenticate_user(
    user_login: UserLogin, reader: Annotated[UserReader, Depends(get_user_reader)]
) -> UserDTOs:
    user: UserDTOs | None = await reader.get_user_by_username(user_login.username)
    if user is None:
        raise AuthBadCredentialsException()

    if not pwd_context.verify(user_login.password, user.password):
        raise AuthBadCredentialsException()

    return user


async def create_token_pair(
    user_id: uuid.UUID,
    scopes: list[str],
) -> Token:
    access = generate_token(
        data={"sub": str(user_id), "scopes": scopes}, expires_delta=ACCESS_TOKEN_EXPIRES
    )
    refresh = generate_token(
        data={"sub": str(user_id), "scopes": scopes},
        expires_delta=REFRESH_TOKEN_EXPIRES,
    )
    # TODO: await
    #       cache_repo.
    #       set_token(
    #           form_data.username,
    #           refresh_token,
    #           refresh_expires_time
    #           )
    return Token(
        access_token=access,
        refresh_token=refresh,
        token_type="bearer",
    )


def generate_token(
    data: dict[str, str | datetime | list[str]], expires_delta: timedelta | None = None
) -> str:
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(
        to_encode, config.auth.secret_key, algorithm=config.auth.algorithm
    )
    return cast(str, encoded_jwt)


async def validate_token(token: Annotated[str, Depends(oauth2_scheme)]) -> TokenData:
    try:
        payload = jwt.decode(
            token, config.auth.secret_key, algorithms=[config.auth.algorithm]
        )
        user_id = payload.get("sub")
        if user_id is None:
            raise InvalidTokenException

        scopes = payload.get("scopes")
        if scopes is None:
            raise InvalidTokenException

        return TokenData(id=user_id, scopes=scopes)
    except JWTError:
        raise InvalidTokenException


async def valid_refresh_token(refresh_token: str) -> Token:
    payload = jwt.decode(
        refresh_token, config.auth.secret_key, algorithms=[config.auth.algorithm]
    )

    user_id = payload.get("sub")
    token = ""
    # TODO: token = await cache_repo.get_token(username)
    if token is None:
        raise InvalidTokenException

    scopes = payload.get("scopes")
    return await create_token_pair(user_id=user_id, scopes=scopes)
