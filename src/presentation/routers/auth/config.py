from pydantic import BaseModel


class AuthConfig(BaseModel):
    secret_key: str = "My18WRuCFRpxonsVeu4sJw"
    algorithm: str
    access_token_expire_minutes: int
    refresh_token_expire_minutes: int
