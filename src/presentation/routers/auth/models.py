import uuid
from typing import cast

from src.application.pydantic.base_model import Base as BaseModel
from pydantic import validator, root_validator, Field

from src.infra.security.security import pwd_context
from src.presentation.config import config


class Token(BaseModel):
    access_token: str
    refresh_token: str
    token_type: str


class TokenData(BaseModel):
    id: uuid.UUID | None = None
    scopes: list[str] = Field(default_factory=list)


class UserCreate(BaseModel):
    username: str
    email: str
    password: str

    @validator("password")
    def hash_password(cls, v: str) -> str:
        return cast(str, pwd_context.hash(v, salt=config.auth.secret_key))


class UserLogin(BaseModel):
    username: str | None = None
    email: str | None = None
    password: str

    @root_validator
    def check_username_or_email(cls, values: dict[str, str]):
        assert values.get("username") is not None or values.get("email") is not None
        return values
