from fastapi import APIRouter
from starlette import status
from starlette.responses import RedirectResponse

router = APIRouter(
    prefix="",
    include_in_schema=False,
)


@router.get("/")
async def default_request() -> RedirectResponse:
    return RedirectResponse("/docs", status_code=status.HTTP_302_FOUND)
