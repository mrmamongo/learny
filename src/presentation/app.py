from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from src.presentation.config import config
from src.application.lifespan import lifespan
from src.presentation.routers.default.v1 import router as default_router
from src.presentation.routers.auth.v1 import router as auth_router
from src.presentation.routers.course.v1 import router as course_router
from src.presentation.routers.users.v1 import router as users_router


def get_application() -> FastAPI:
    _app = FastAPI(title=config.api.project_name, lifespan=lifespan)

    _app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin) for origin in config.api.backend_cors_origins],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    _app.include_router(default_router, tags=["default"], include_in_schema=False)
    _app.include_router(auth_router, prefix=config.api.prefix, tags=["auth"])
    _app.include_router(users_router, prefix=config.api.prefix, tags=["users"])
    _app.include_router(course_router, prefix=config.api.prefix, tags=["courses"])

    return _app
