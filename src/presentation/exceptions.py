import logging

from fastapi import FastAPI, Request, status
from fastapi.responses import ORJSONResponse

from src.application.common.exceptions import ApplicationException
from src.application.user.exceptions import (
    UserIdNotExist,
    UsernameNotExist,
    UserIdAlreadyExists,
    UsernameAlreadyExists,
    EmailAlreadyExists,
)
from src.presentation.models import ErrorResult

logger = logging.getLogger(__name__)


def setup_exception_handlers(app: FastAPI) -> None:
    app.add_exception_handler(UserIdNotExist, user_id_not_exist_handler)
    app.add_exception_handler(UsernameNotExist, username_not_exist_handler)
    app.add_exception_handler(UserIdAlreadyExists, user_id_already_exists_handler)
    app.add_exception_handler(UsernameAlreadyExists, username_already_exists_handler)
    app.add_exception_handler(EmailAlreadyExists, email_already_exists_handler)
    app.add_exception_handler(Exception, unknown_exception_handler)


async def user_id_not_exist_handler(
    request: Request, err: UserIdNotExist
) -> ORJSONResponse:
    return await handle_error(request, err, status_code=status.HTTP_404_NOT_FOUND)


async def username_not_exist_handler(
    request: Request, err: UsernameNotExist
) -> ORJSONResponse:
    return await handle_error(request, err, status_code=status.HTTP_404_NOT_FOUND)


async def user_id_already_exists_handler(
    request: Request, err: UserIdAlreadyExists
) -> ORJSONResponse:
    return await handle_error(request, err, status_code=status.HTTP_409_CONFLICT)


async def username_already_exists_handler(
    request: Request, err: UsernameAlreadyExists
) -> ORJSONResponse:
    return await handle_error(request, err, status_code=status.HTTP_409_CONFLICT)


async def email_already_exists_handler(
    request: Request, err: EmailAlreadyExists
) -> ORJSONResponse:
    return await handle_error(request, err, status_code=status.HTTP_404_NOT_FOUND)


async def unknown_exception_handler(request: Request, err: Exception) -> ORJSONResponse:
    logger.error("Handle error", exc_info=err, extra={"error": err})
    logger.exception("Unknown error occurred", exc_info=err, extra={"error": err})
    return ORJSONResponse(
        ErrorResult(message="Unknown server error has occurred", data=err),
        status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
    )


async def handle_error(
    request: Request, err: ApplicationException, status_code: int
) -> ORJSONResponse:
    logger.error("Handle error", exc_info=err, extra={"error": err})
    return ORJSONResponse(
        ErrorResult(message=err.message, data=err),
        status_code=status_code,
    )
